import os
from typing import TYPE_CHECKING
import scraper
import requests
from datetime import date
import json
import dogbox

url = "https://api.blocket.se/search_bff/v1/content?cg=6081&lim=40&q=hund&r=11&st=s&include=all&gl=3&include=extend_with_shipping"
token = os.getenv('BLOCKET_API_TOKEN', 'no blocket api key')

def get_blocket_dogs():
  response = requests.get(url, headers={
    "User-agent": "Mozilla/5.0",
    "Authorization": "Bearer " + token
  })

  result = response.content
  return result

def calculate_prices(dogs):
  print("calculate prices")
  sum = 0
  dog_amount = 0
  max_price = 0
  min_price = 0
  median_price = 0
  dog_prices = []
  count = 0
  for dog in dogs:
    try:
      dog_price = int(dog["price"]["value"])
      dog_prices.append(dog_price)
      count=count+1
      sum = sum + dog_price
      dog_amount = dog_amount + 1
      
      if dog_price > max_price:
        max_price = dog_price
      
      if dog_price < min_price:
        min_price = dog_price

    except: count=count
  
  median_price = calculate_median(dog_prices)

  mean = sum/dog_amount
  return mean, min_price, max_price, median_price

def sort(list):
  for iter_num in range(len(list)-1,0,-1):
    for idx in range(iter_num):
      if list[idx]>list[idx+1]:
        temp = list[idx]
        list[idx] = list[idx+1]
        list[idx+1] = temp
  return list


def calculate_median(arr):
  arr = sort(arr)
  
  if len(arr) % 2 == 0:
    median_id_low = (int)(len(arr) / 2)
    result = (arr[median_id_low] + arr[median_id_low])/2
    print(result)
    return result
  else:
    median_id = (int)(len(arr) / 2)
    result = arr[median_id]
    print(result)
    return result


def main():
  
  result = get_blocket_dogs()
  dict = json.loads(result)
  mean, min_price, max_price, median_price = calculate_prices(dict["data"])
  dog_amount = len(dict["data"])
  
  today = date.today().strftime("%Y%m%d")

  result = { 
    "date": today, 
    "amount": dog_amount, 
    "mean price": mean, 
    "max price": max_price, 
    "min price": min_price, 
    "median price": median_price 
  }

  print("today", today)
  print("dog_amount", dog_amount)
  print("mean", mean)
  print("max_price", max_price)
  print("min_price", min_price)
  print("median_price", median_price)

  
  # dogbox.write_csv([result], ["date","amount","mean price"], "/hundar/blocket.csv",";")


  
if __name__ == "__main__":
  main()
