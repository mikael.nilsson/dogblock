# DOGBLOCK
A script to check the prices of dogs on blocket.se

As "we're" looking into getting a dog, we discussed the prices of dogs on blocket.se, especially since we got the feeling that they had increased during 2020. So this script calculates the mean price and the amount of dogs and saves in a csv on dropbox together with the date.


## Updates
[2022-03-13]:
As The New Adventurer has arrived to the pack, this code is no longer that needed in itself, but it's still a nice little project and I'm still kind of curious about the prices, so perhaps I'll do some more stuff on this thingie.

[2021-04-18]:
There is a token needed, but it is exchanged every 15 minutes and I haven't yet seen how to get that token. I will need to find a way of collecting the token from blocket.se. What I've seen is the token is used firstly as bearer on the second chirp call, I haven't seen it returned before so probably one will have to scrape that call and pick the header.

### Installing dependencies
pipenv install

### Running script
pipenv run python -m main.py

### Running tests
pipenv run python -m nose2
